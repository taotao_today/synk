# Go局域网终端互传文件

## 情景

基于真实的场景：我不想打开手机和微信（不想处理不太紧急的事情）

学习Go基础，想要做出一些东西来，不管是什么项目都行，抛开书和视频开始做项目，在项目中学习。



### 如何选项目？

**牛逼的开源项目：**

不具备独自看懂的能力

**复杂的商业项目：**

没半年时间是搞不定的

**剩下的只有小项目：**

可能存在的问题，面试官可能不太喜欢



### 任何东西是不能一步到位的

你必须先有能力**搞定几个小项目**，才有可能**做成一个大项目**

你必须先**做毁**一个小项目，才有可能**做成**一个大项目



## 需求说明

完全是我个人的需求：

1.  怎么把电脑上的文件传给手机？
2.  怎么把手机上的文件传给电脑？

正常人类的答案：微信、QQ、钉钉

我的答案：自己做一个

>   因为在做事情时，需要集中注意力，但是拿起手机有时候就会控制不住自己；比如只想会个消息，但是就一不小心忘记了自己原来的目的，做了其他时。而且打开微信和钉钉会有很多人来找我，我就把微信和QQ关掉，那他们就不能找到我吗？他们还是能找到我，但是不那么紧急的事情就会暂时远离我。这样我就可以更专注做手头的工作

不想开打开微信、不想开蓝牙、不想注册账号、最多扫个二维码



## 软件架构

![1646842704315](Go局域网终端互传文件.assets/1646842704315.png)



## 用到的工具

IDE: GoLand

zserge/lorca：一个制作桌面应用的库

React：前端相关代码，找队友完成

gin-gonic/gin：提供服务器接口

gorilla/websocket：实现websocket通知

skip2/go-qrcode：生成二维码



### 其他非必须

版本：1.17或以上

安装gowatch：热启动代码

`go get github.com/silenceper/gowatch`



## lorca

调用本地的Chrome浏览器

看懂文档即可





## 开始制作项目

### 创建项目

`go mod init github.com/taoao_today/sync`

### 引入lorca：调用本地的Chrome浏览器

```go
package main

import (
	"os"
	"os/signal"
	"syscall"

	"github.com/zserge/lorca"
)

func main() {
	var ui lorca.UI
    //构造args参数，创建Chrome浏览器，同时创建一个协程监听Chrome关闭
	ui, _ = lorca.New("https://baidu.com", "", 800, 600, "--disable-sync", "--disable-translate")
    // go监听interrupt
	chSignal := make(chan os.Signal, 1)
    // Notify订阅，如果用户发送中断信号，就通知chSignal这个channel
	signal.Notify(chSignal, syscall.SIGINT, syscall.SIGTERM)
    // 阻塞程序，等待筛选channel 
	select {
	case <-ui.Done():
	case <-chSignal:
	}
	ui.Close()
}

```

关闭UI后，主线程会自动退出

中断主线程后，UI会自动退出

声明一个`interface`，没给它赋值，它就是`nil`

## 查看select源码

**随机轮询，一成就结束**

源代码：https://go.dev/src/runtime/select.go

源代码：https://go.dev/src/runtime/proc.go

​	

go是自举的，所以可以看它所有源码



### 把自己永远挂起：永远gopark

```go
select{}
```

与死循环对比

```go
for{

}
```

注意：select会导致当前协程进入睡眠，如果所有协程都处于睡眠，go会用fatal来处理



## 协程

go来协助的线程，所以协程是一种线程



## 使用exec.Command启动Chrome(读lorca源码)

```go
chromePath := "C:\\Users\\today\\AppData\\Local\\Google\\Chrome\\Application\\chrome.exe"
cmd := exec.Command(chromePath, "--app=http://127.0.0.1:8081/")
cmd.Start()

cmd.Process.Kill()
```



## gin

```go
go func() {
    // 写代码时用debug模式，发布时用生产模式
    gin.SetMode(gin.DebugMode)
    // 新建一个路由
    router := gin.Default()
    // 建立一个路由规则;*gin.Context是gin的上下文对象，封装了request和response
    router.GET("/", func(c *gin.Context) {
        c.Writer.Write([]byte("测试程序"))
    })
    // 监听8080端口，来一个连接就开一个协程，查看源码 go c.serve(connCtx)
    router.Run(":8080")
}()
```

注意使用协程运行，也就是用go关键字运行它；因为我们后面还要用代码启动Chrome



## 思考：

有没有可能gin还没启动，UI就访问页面了



**回答：**不太可能，因为开启一个进程（Chrome）和开启一个协程所消耗的资源是相差巨大的



>   对于会自然消亡的事物，生命每增加一天，其预期寿命就会缩短一些。
>
>   而对于不会自然消亡的事物，生命每增加一天，则可能意味着更长的预期剩余寿命。

**解读：**

人每多活一天，寿命就少一天。

但是`go`语言，没多存在一天，它就会更多的运用在项目中，就会存在更多的寿命；所以`go`永远不能真正==取代==`Java`，因为开发一个`spring`框架来也没用，因为现有项目迁移的困难和成本是巨大的。还不如新开一个坑，引导人们去填。比如说：云开发。



## 监听Ctrl + C中断信号

```go
chSignal := make(chan os.Signal, 1)
signal.Notify(chSignal, syscall.SIGINT, syscall.SIGTERM)
select {
    case <-ui.Done():
	case <-chSignal:
    }
ui.Close()
```



## 添加静态文件路由

```go
/*
	在打包exe文件时，把 frontend/dist/* 一起打包进去
*/

//go:embed frontend/dist/*
var FS embed.FS
```

打包部署时非常方便的工具。



```go
// 添加静态路由
router.StaticFS("/static", http.FS(staticFiles))

// 如果前面添加的路由都没有匹配，就会执行NoRoute
router.NoRoute(func(c *gin.Context) {
    // 获取用户访问的路径
    path := c.Request.URL.Path
    // 如果路径是以 /static/ 开头的就说明是访问静态文件的
    if strings.HasPrefix(path, "/static/") {
        // 打开index.html文件
        reader, err := staticFiles.Open("index.html")
        if err != nil {
            // Fatal致命的错误
            log.Fatal(err)
        }
        defer reader.Close()
        // 获取index.html的文件长度
        stat, err := reader.Stat()
        if err != nil {
            log.Fatal(err)
        }
        // 读文件，并将内容封装成一个HTTP Stream
        c.DataFromReader(http.StatusOK, stat.Size(), "text/html;charset=utf-8", reader, nil)
    } else {     // 否则返回404
        c.Status(http.StatusNotFound)
    }
})
```



## 实现接口1：上传文本

POST    /api/v1/texts

### 思路：

1.  获取`go`**执行文件**所在目录
2.  在该目录创建uploads目录
3.  将文本保存为一个文件（使用uuid生成一个随机文件名）
4.  返回该文件的下载路径



### TextsController 处理上传文本保存

```go
// TextsController 处理上传文本保存
func TextsController(c *gin.Context) {
	var json struct {
		Raw string `json:"raw"`
	}
	// 将空的结构体按照指定格式填充
	if err := c.ShouldBindJSON(&json); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	} else {
		exe, err := os.Executable() // 获取当前执行文件的路径
		if err != nil {
			log.Fatal(err)
		}
		dir := filepath.Dir(exe) // 获取当前执行文件的目录
		if err != nil {
			log.Fatal(err)
		}
		filename := uuid.New().String()          // 生成一个文件名
		uploads := filepath.Join(dir, "uploads") // 拼接 uploads 的绝对路径
		err = os.MkdirAll(uploads, os.ModePerm)  // 递归创建 uploads 目录，os.ModePerm是文件权限

		fmt.Println(uploads)

		if err != nil {
			log.Fatal(err)
		}
		fullpath := path.Join("uploads", filename+".txt")                            // 拼接文件的绝对路径（不含 exe 所在目录）
		err = ioutil.WriteFile(filepath.Join(dir, fullpath), []byte(json.Raw), 0644) // 将 json.Raw 写入文件
		if err != nil {
			log.Fatal(err)
		}
		c.JSON(http.StatusOK, gin.H{"url": "/" + fullpath}) // 返回文件的绝对路径（不含 exe 所在目录）
	}

}
```



### 添加路由

```go
router.POST("/api/v1/texts", TextsController)
```



## 实现接口2：获取局域网IP

GET  /api/v1/addresses

### 思路：

1.  获取电脑在各个局域网的IP地址
2.  转为JSON写入HTTP响应



### AddressesController 获取局域网IP

```go
func AddressesController(c *gin.Context) {
	// 获取当前电脑在各个局域网的IP地址
	addrs, _ := net.InterfaceAddrs()
	var result []string
	// 转为JSON写入HTTP相应
	for _, address := range addrs {
		// 对address进行断言，断言它是一个*net.IPNet类型
		if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				result = append(result, ipnet.IP.String())
			}
		}
	}
	c.JSON(http.StatusOK, gin.H{"addresses": result})
}
```

### 添加路由

```go
router.GET("/api/v1/addresses", AddressesController)
```



## 实现接口3：文件下载

GET  /uploads/:path

### 思路：

1.  将网络路径 :path变成本地绝对路径
2.  读取本地文件，写到HTTP响应里



### UploadsController

```go
// GetUploadsDir 拼接 uploads 的绝对路径
func GetUploadsDir() (uploads string) {
	exe, err := os.Executable()
	if err != nil {
		log.Fatal(err)
	}
	dir := filepath.Dir(exe)
	uploads = filepath.Join(dir, "uploads")
	return
}

func UploadsController(c *gin.Context) {
	if path := c.Param("path"); path != "" {
		target := filepath.Join(GetUploadsDir(), path)
		c.Header("Content-Description", "File Transfer")
		c.Header("Content-Transfer-Encoding", "binary")
		c.Header("Content-Disposition", "attachment; filename="+path)
		c.Header("Content-Type", "application/octet-stream")
		c.File(target)
	} else {
		c.Status(http.StatusNotFound)
	}
}
```

### 添加路由

```go
router.GET("/uploads/:path", UploadsController)
```



## 实现接口3：创建二维码

GET  /api/v1/qrcodes

### 思路：

1.  获取文本内容
2.  将文本转为图片（用第三方库）
3.  将图片写入HTTP响应

>   符合单一职责原则；保证接口的通用性。

### QrcodesController

```go
func QrcodesController(c *gin.Context) {
	// 获取文本内容
	if content := c.Query("content"); content != "" {
		// 将文本转为图片
		png, err := qrcode.Encode(content, qrcode.Medium, 256)
		if err != nil {
			log.Fatal(err)
		}
		// 将图片写入HTTP响应
		c.Data(http.StatusOK, "image/png", png)
	} else {
		c.Status(http.StatusBadRequest)
	}
}
```

### 添加路由

```go
router.GET("/api/v1/qrcodes", QrcodesController)
```



## 实现接口5：上次文件

POST  /api/v1/files

跟POST   /api/v1/texts类似

区别在于保存文件而不是保存文本

### 思路：

1.  获取go执行文件所在目录
2.  在该目录创建uploads目录
3.  将文件保存为另一个文件
4.  返回后者的下载路径



### FilesController

```go
func FilesController(c *gin.Context) {
    // 获取前端传来的文件
	file, err := c.FormFile("raw")
	if err != nil {
		log.Fatal(err)
	}
    // 拿到可执行文件
	exe, err := os.Executable()
	if err != nil {
		log.Fatal(err)
	}
    // 获取go执行文件所在目录
	dir := filepath.Dir(exe)
	if err != nil {
		log.Fatal(err)
	}
    // 使用uuid随机生成一个文件名
	filename := uuid.New().String()
    // 拼接uploads路径
	uploads := filepath.Join(dir, "uploads")
    // 递归创建uploads目录
	err = os.MkdirAll(uploads, os.ModePerm)
	if err != nil {
		log.Fatal(err)
	}
    // 获取本地绝对路径
	fullpath := path.Join("uploads", filename+filepath.Ext(file.Filename))
    // 将文件保存为另一个文件
	fileErr := c.SaveUploadedFile(file, filepath.Join(dir, fullpath))
	if fileErr != nil {
		log.Fatal(fileErr)
	}
	c.JSON(http.StatusOK, gin.H{"url": "/" + fullpath})
}
```

SaveUploadedFile：每次读一部分文件内容，然后再写进去。

分片上次，HTTP1.1及以上的版本支持



### 路由

```go
router.POST("/api/v1/files", FilesController)
```





## websocket

sever 与 client沟通一般都使用http进行请求响应。但是http是**无状态**的，在你来的时候sever其实是不知道你是谁的。我请求你响应，响应完了就可以把内存清理掉了。



socket是建立一条长连接，两者都可以收发消息。效率优于HTTP，因为它不需要填写HTTPheader。socket没有对具体内容的要求，所以减少了解析的过程。缺点是sever和client都需要维护一块内存（对方的状态或者结构体），表示与sever有一条连接。所以socket是**持久化连接**。



websocket是两者的结合起来，浏览器只会发HTTP请求。所以我们要想与server建立连接，就需要协议升级的过程，给sever发送一个请求，说我想要websocket。

```
wss://xxxxxxxx.........
```

服务器会返回一个**101**的响应码，`Switching Protocols`



###  发布订阅模式

```
eventHub.trigger     #触发事件
eventHub.on          #监听事件
eventHub.off         #取消监听
```



```go
type Hub struct {
	clients    map[*Client]bool       #统计有多少人在监听
	broadcast  chan []byte            #触发事件
	register   chan *Client           #监听事件  
	unregister chan *Client           #取消监听
}
```



### run

```go
func (h *Hub) Run() {
	for {        
		select {
        // 如果有人监听，就把它放在client里面
		case client := <-h.register:
			h.clients[client] = true
        // 如果有人取消注册了，就把它从client里面删掉
		case client := <-h.unregister:
			if _, ok := h.clients[client]; ok {
				delete(h.clients, client)
				close(client.send)
			}
        // 如果有广播消息，就遍历所有客户，把消息发送给客户
		case message := <-h.broadcast:
			for client := range h.clients {
				select {
				case client.send <- message:
				default:
					close(client.send)
					delete(h.clients, client)
				}
			}
		}
	}
}
```



### severWs

```go
func wshandler(hub *Hub, w http.ResponseWriter, r *http.Request) {
    // 协议升级
	conn, err := wsupgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}
    // 初始化一个client信息，Client是一个结构体，如果不加&它就会不停的深拷贝，就不能固定一个client了
	client := &Client{hub: hub, conn: conn, send: make(chan []byte, 256)}
    // 注册一下用户
	client.hub.register <- client
	// 不停的读和写
	go client.writePump()
	go client.readPump()
}
```



### 添加路由

```go
hub := ws.NewHub()
go hub.Run()
router.GET("/ws", func(c *gin.Context) {
		ws.HttpController(c, hub)
	})
```

